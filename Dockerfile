FROM node:16 AS builder
WORKDIR /app      
COPY ./package*.json ./
RUN npm install
COPY . .
RUN npm run build

FROM nginx:latest
EXPOSE 3001
COPY ./default.conf /etc/nginx/conf.d/default.conf 
COPY --from=builder /app/build  /usr/share/nginx/html 
